package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity

public class images {
	images()
	{
	}
	@Id
	
	private int imageId;
	
	private String imageName;
	
	private String imageUrl;
	public int getimageId() {
		return imageId;
	}
	public void setimageId(int imageId) {
		this.imageId = imageId;
	}
	public String getimageName() {
		return imageName;
	}
	public void setimageName(String imageName) {
		this.imageName = imageName;
	}
	public String getimageUrl() {
		return imageUrl;
	}
	public void setimageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	@Override
	public String toString() {
		return "images [imageId=" + imageId + ", imageName=" + imageName + ", imageUrl=" + imageUrl + "]";
	}
}
