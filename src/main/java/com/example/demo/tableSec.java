package com.example.demo;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(tableSec.class)
public class tableSec implements java.io.Serializable{

	@Id	
	
	@ManyToOne(targetEntity= docSection.class )
	@JoinColumn
	private List<docSection> secId;	
		
	@ManyToOne(targetEntity= table1.class )
	@JoinColumn
	private List<table1> tableId;
}
