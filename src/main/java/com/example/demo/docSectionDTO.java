package com.example.demo;

import java.util.List;

public class docSectionDTO {

	private int secId;	
	private List<Documents> docCode;		
	
	private String secSequence;	
	
	private String secCode;		
	
	private String secTitle;
		
	private String secText;

	public int getsecId() {
		return secId;
	}

	public void setsecId(int secId) {
		this.secId = secId;
	}

	public List<Documents> getdocCode() {
		return docCode;
	}

	public void setdocCode(List<Documents> docCode) {
		this.docCode = docCode;
	}

	public String getsecSequence() {
		return secSequence;
	}

	public void setsecSequence(String secSequence) {
		this.secSequence = secSequence;
	}

	public String getsecCode() {
		return secCode;
	}

	public void setsecCode(String secCode) {
		this.secCode = secCode;
	}

	public String getsecTitle() {
		return secTitle;
	}

	public void setsecTitle(String secTitle) {
		this.secTitle = secTitle;
	}

	public String getsecText() {
		return secText;
	}

	public void setsecText(String secText) {
		this.secText = secText;
	}

	@Override
	public String toString() {
		return "doc_sectionDTO [secId=" + secId + ", docCode=" + docCode + ", secSequence=" + secSequence
				+ ", secCode=" + secCode + ", secTitle=" + secTitle + ", secText=" + secText + "]";
	}
	

}
