package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;



@Entity
public class table1 {
	table1(){
	}
	
	@Id
	
	private int tableId;
	
	private String tableName;
	
	private String rows1;
	
	private String column1;
	public int gettableId() {
		return tableId;
	}
	public void settableId(int tableId) {
		this.tableId = tableId;
	}
	public String gettableName() {
		return tableName;
	}
	public void settableName(String tableName) {
		this.tableName = tableName;
	}
	public String getrows1() {
		return rows1;
	}
	public void setrows1(String rows1) {
		this.rows1 = rows1;
	}
	public String getcolumn1() {
		return column1;
	}
	public void setcolumn1(String column1) {
		this.column1 = column1;
	}
	@Override
	public String toString() {
		return "table [tableId=" + tableId + ", tableName=" + tableName + ", rows1=" + rows1 + ", column1=" + column1
				+ "]";
	}
	

}

