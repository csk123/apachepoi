package com.example.demo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class docSection {
	docSection(){
	}
	
	@Id		
	
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private int secId;		
	/*
	 * @ManyToOne( targetEntity= Documents.class )
	 * 
	 * @JoinColumn private List<Documents> docCode;
	 */		
	
	private String secSequence;	
	
	private String secCode;		
		
	private String secTitle;
		
	private String secText;
	public int getsecId() {
		return secId;
	}
	public void setsecId(int secId) {
		this.secId = secId;
	}

	/*
	 * public List<Documents> getdocCode() { return docCode; } public void
	 * setdocCode(List<Documents> docCode) { this.docCode = docCode; }
	 */
	public String getsecSequence() {
		return secSequence;
	}
	public void setsecSequence(String secSequence) {
		this.secSequence = secSequence;
	}
	public String getsecCode() {
		return secCode;
	}
	public void setsecCode(String secCode) {
		this.secCode = secCode;
	}
	public String getsecTitle() {
		return secTitle;
	}
	public void setsecTitle(String secTitle) {
		this.secTitle = secTitle;
	}
	public String getsecText() {
		return secText;
	}
	public void setsecText(String secText) {
		this.secText = secText;
	}
	@Override
	public String toString() {
		return "doc_section [secId=" + secId + /* ", docCode=" + docCode + */", secSequence=" + secSequence
				+ ", secCode=" + secCode + ", secTitle=" + secTitle + ", secText=" + secText + "]";
	}
	
	
}
