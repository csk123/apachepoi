package com.example.demo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@IdClass(imgSec.class)
public class imgSec implements Serializable {
	@Id		
	@ManyToOne(targetEntity= docSection.class )
	@JoinColumn
	private List<docSection> secId;	

	@ManyToOne(targetEntity= images.class )
	@JoinColumn
	private List<images> imageId;

}
