package com.example.demo;
import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Documents {
	
	
		Documents(){
		}
		
		@Id				
		@GeneratedValue(strategy = GenerationType.AUTO)	
		private int docCode;	
	/*
	 * @ManyToOne(targetEntity= docType.class )
	 * 
	 * @JoinColumn private List<docType> docTypeCode;
	 */	
		
		private String docName;	
			
		private String docDescription;		
			
		private String docStructureDescription;		
		public int getdocCode() {
			return docCode;
			}
		public void setdocCode(int docCode) {
			this.docCode = docCode;
			}		

	/*
	 * public List<docType> getdocTypeCode() { return docTypeCode; } public void
	 * setdocTypeCode(List<docType> docTypeCode) { this.docTypeCode = docTypeCode; }
	 */
		public String getdocName() {
			return docName;
			}
		public void setdocName(String docName) {
			this.docName = docName;	
			}
		public String getdocDescription() {
			return docDescription;		
			}		
		public void setdocDescription(String docDescription) {
			this.docDescription = docDescription;	
			}
		public String getdocStructureDescription() {		
			return docStructureDescription;		
			}
		public void setdocStructureDescription(String docStructureDescription) {
			this.docStructureDescription = docStructureDescription;		
			}
		@Override
		public String toString() {			
		return "Documents [docCode=" + docCode + /* ", docTypeCode=" + docTypeCode + */ ", docName=" + docName					+ ", docDescription=" + docDescription + ", docStructureDescription="					+ docStructureDescription + "]";		}
}
